<?php

namespace Guard;

class Collection extends Rule
{
    private $fieldName;
    private $itemRule;
    private $bypass = false;

    final public function __construct(string $fieldName = null, Item $itemRule)
    {
        $this->fieldName = $fieldName;
        $this->itemRule = $itemRule;

        $this->check(function ($data) {
            if (!is_array($data)) {
                $this->bypass = true;

                return 'This value must be an array.';
            }
        });
    }

    final public function length(int $min, int $max) : Collection
    {
        $this->check(function ($data) use ($min, $max) {
            if ($this->bypass) {
                return;
            }

            if ($min > $count = count($data)) {
                return "This value must contain at least {$min} element(s).";
            }

            if ($max < $count) {
                return "This value must contain at most {$max} element(s).";
            }
        });

        return $this;
    }

    final public function _field(): string
    {
        return $this->fieldName;
    }

    final public function _itemRule(): Item
    {
        return $this->itemRule;
    }
}
