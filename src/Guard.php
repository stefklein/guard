<?php

namespace Guard;

class Guard
{
    /**
     * @param array $data
     * @param RuleInterface[] $rules
     * @return Result
     */
    public function inspect(array $data, array $rules) : Result
    {
        $finalData = $data;
        $errors = [];

        foreach ($rules as $rule) {
            if (!array_key_exists($field = $rule->_field(), $data)) {
                // first handle missing values
                if (!$rule->_required()) {
                    $finalData[$field] = $rule->_defaultValue();
                } else {
                    $errors[$field][] = 'This value is required.';
                }
            } else {
                // if a value is present, apply all checks on it
                foreach ($rule->_checks() as $check) {
                    if ($error = $check($data[$field])) {
                        if (!is_string($error)) {
                            throw new \Exception(
                                "Rule error must be a string (checking field '{$field}')"
                            );
                        }

                        $errors[$field][] = $error;
                    }
                }

                if ($rule instanceof Collection && is_array($data[$field])) {
                    foreach ($data[$field] as $index => $item) {
                        foreach ($rule->_itemRule()->_checks() as $check) {
                            if ($error = $check($item, $index)) {
                                if (!is_string($error)) {
                                    throw new \Exception(
                                        "Rule error must be a string (checking field '{$field}/{$index}')"
                                    );
                                }

                                $errors["{$field}/{$index}"][] = $error;
                            }
                        }

                        if (!isset($errors[$field][$index])) {
                            foreach ($rule->_itemRule()->_transforms() as $transform) {
                                // transform is made on final data array to allow chaining
                                $finalData[$field][$index] = $transform($finalData[$field][$index], $index);
                            }
                        }
                    }
                }

                // if a value is present and valid, apply transforms on it
                if (!isset($errors[$field])) {
                    foreach ($rule->_transforms() as $transform) {
                        // transform is made on final data array to allow chaining
                        $finalData[$field] = $transform($finalData[$field]);
                    }
                }
            }
        }

        return new Result($finalData, $errors);
    }
}
