<?php

namespace Guard;

class Item extends Rule
{
    private $fieldName;

    final public function __construct(string $fieldName = null)
    {
        $this->fieldName = $fieldName;
    }

    final public function _field(): string
    {
        return $this->fieldName;
    }
}
