<?php

namespace Guard;

abstract class Rule implements RuleInterface
{
    private $defaultValue;
    private $required = true;
    private $checks = [];
    private $transforms = [];

    abstract public function _field(): string;

    final public function ifNone($defaultValue) : RuleInterface
    {
        $this->required = false;
        $this->defaultValue = $defaultValue;

        return $this;
    }

    final public function check(callable $checkFunction) : RuleInterface
    {
        $this->checks[] = $checkFunction;

        return $this;
    }

    final public function transform(callable $transformFunction) : RuleInterface
    {
        $this->transforms[] = $transformFunction;

        return $this;
    }

    final public function _required(): bool
    {
        return $this->required;
    }

    final public function _defaultValue()
    {
        return $this->defaultValue;
    }

    final public function _checks() : array
    {
        return $this->checks;
    }

    final public function _transforms(): array
    {
        return $this->transforms;
    }
}
