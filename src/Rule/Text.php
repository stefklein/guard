<?php

namespace Guard\Rule;

use Guard\RuleInterface;

class Text implements RuleInterface
{
    public function __construct()
    {

    }

    public function _sanitize($data)
    {
        return $data;
    }

    public function _validate($data) : array
    {
        return [];
    }
}
