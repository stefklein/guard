<?php

namespace Guard;

interface RuleInterface
{
    public function ifNone($defaultValue) : RuleInterface;
    public function check(callable $checkFunction) : RuleInterface;
    public function transform(callable $transformFunction) : RuleInterface;
    public function _field() : string;
    public function _required() : bool;
    public function _defaultValue();
    public function _checks() : array;
    public function _transforms() : array;
}
