<?php

namespace Guard\Tests;

use Guard\Guard;
use Guard\Rule as C;
use Guard\Result;
use PHPUnit\Framework\TestCase;

class GuardBup extends TestCase
{
    public function test()
    {
        // rejeter data !== array ?

        $guard = new Guard();
        $result = $guard->inspect('foo', C::text());
        $this->assertTrue($result->isValid());
        $this->assertEquals('foo', $result->getData());

        // validation (valid)
        $guard->inspect('foo', C::text()->length(2, 10));
        $guard->inspect('foo@bar.baz', C::email());
        $guard->inspect(1234, C::zipCode());
        $guard->inspect([], C::arrayOf(C::text()));
        $guard->inspect(['foo', 'bar'], C::arrayOf(C::text()));

        // AbstractConstraint: unique, ifNone -> final

        // Special constraints (final): coll, dict, custom

        $guard->inspect('[...]', [
            C::text('firstName')->length(3, 25),
            C::text('lastName')->length(5, 18),
            C::text('nickname')->length(5, 18)->ifNone('toto'),
            C::email('email')->unique(function ($email) {
                return true; // e.g. db lookup failed
            }),
            C::text('description')->trim(false)->ifNone('[No description]'),
            C::coll('hobbies')->items(
                C::text()->length(2, 10)
            )->ifNone([]),
            C::dict('address')->entries([
                C::text('firstLine')->length(3, 100),
                C::text('secondLine')->length(3, 100)->ifNone(null),
                C::text('city')->length(3, 100),
                C::zipCode('zip'),
                C::countryCode('cc'),
                C::custom(function (array $address) {
                    if (isset($address['cc']) && $address['cc'] === 'BE') {
                        return [
                            C::item('city')->in(['Bruxelles', 'Namur']),
                            C::zipCode('zip')->country('BE')
                        ];
                    }

                    return C::fail('cc')->message('Country not accepted...');
                })
            ])
        ]);

        // validation (invalid)
        $guard->inspect(null, C::text());
        $guard->inspect(123, C::text()->length(2, 10));
        $guard->inspect('foo', C::text()->length(5, 8));

        // defaults
        $guard->inspect(null, C::text()->ifNone('bar')); // 'bar'
        $guard->inspect('', C::text()->ifNone('baz')); // 'baz'
        $guard->inspect(null, C::arrayOf(C::text())->ifNone([])); // []
        $guard->inspect([], C::arrayOf(C::text())->ifEmpty(['bar'])); // ['bar']

        // sanitization
        $guard->inspect(' foo  ', C::text()); // 'foo'
        $guard->inspect('<script>alert("foo")</script>', C::text()); // throws

    }
}

