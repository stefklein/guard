<?php

namespace Guard\Tests;

use Guard\Guard;
use Guard\Collection;
use Guard\Entry;
use Guard\Item;
use PHPUnit\Framework\TestCase;

class GuardTest extends TestCase
{
    /**
     * @dataProvider itemProvider
     */
    public function testItemsAndBasicRuleBehaviour(array $data, array $rules, array $finalData, array $expectedErrors)
    {
        $this->assertRulesAreFollowed($data, $rules, $finalData, $expectedErrors);
    }

    /**
     * @dataProvider collectionProvider
     */
    public function testCollections(array $data, array $rules, array $finalData, array $expectedErrors)
    {
        $this->assertRulesAreFollowed($data, $rules, $finalData, $expectedErrors);
    }

    /**
     * @expectedException \Exception
     * @expectedExceptionMessage Rule error must be a string (checking field 'foo')
     */
    public function testRuleErrorMustBeAString()
    {
        $guard = new Guard();
        $guard->inspect(
            ['foo' => 123],
            [
                (new Item('foo'))->check(function () {
                    return new \stdClass();
                })
            ]
        );
    }

    public function testRuleOutsideCollectionMustBeBoundToAField()
    {
        $this->markTestIncomplete('TODO');
    }

    public function testRuleInsideCollectionMustNotBeBoundToAField()
    {
        $this->markTestIncomplete('TODO');
    }

    public function testCollectionLength()
    {
        // min length >= 0;
        // max length = -1 or >= min;

        $this->markTestIncomplete('TODO');
    }

    public function testCollectionItemCannotHaveADefaultValue()
    {
        $this->markTestIncomplete('TODO');
    }

    public function itemProvider()
    {
        return [
            'empty rules and empty data' => [
                [],
                [],
                [],
                []
            ],
            'required field and empty data' => [
                [],
                [
                    (new Item('foo'))
                ],
                [],
                [
                    'foo' => ['This value is required.']
                ]
            ],
            'default value and empty data' => [
                [],
                [
                    (new Item('foo'))->ifNone(123)
                ],
                [
                    'foo' => 123
                ],
                []
            ],
            'mixed required and default' => [
                [
                    'foo' => 123
                ],
                [
                    (new Item('foo')),
                    (new Item('bar')),
                    (new Item('baz'))->ifNone(456)
                ],
                [
                    'foo' => 123,
                    'baz' => 456
                ],
                [
                    'bar' => ['This value is required.']
                ]
            ],
            'single passing check' => [
                [
                    'foo' => 123
                ],
                [
                    (new Item('foo'))->check(function () {
                        return null;
                    })
                ],
                [
                    'foo' => 123
                ],
                []
            ],
            'missing required data bypasses checks' => [
                [],
                [
                    (new Item('foo'))->check(function () {
                        throw new \Exception('Should be bypassed');
                    })
                ],
                [],
                [
                    'foo' => ['This value is required.']
                ]
            ],
            'single failing check' => [
                [
                    'foo' => 123
                ],
                [
                    (new Item('foo'))->check(function ($value) {
                        return $value > 1 ? 'Too big.' : null;
                    })
                ],
                [
                    'foo' => 123
                ],
                [
                    'foo' => ['Too big.']
                ]
            ],
            'mixed result checks' => [
                [
                    'foo' => 1,
                ],
                [
                    (new Item('foo'))->check(function ($value) {
                        return $value === 1 ? 'Cannot be 1.' : null;
                    }),
                    (new Item('foo'))->check(function ($value) {
                        return $value > 1 ? 'Too big.' : null;
                    }),
                    (new Item('foo'))->check(function ($value) {
                        return is_numeric($value) ? 'Cannot be numeric.' : null;
                    })
                ],
                [
                    'foo' => 1,
                ],
                [
                    'foo' => [
                        'Cannot be 1.',
                        'Cannot be numeric.'
                    ]
                ]
            ],
            'default value bypasses checks' => [
                [],
                [
                    (new Item('foo'))
                        ->check(function () {
                            throw new \Exception('Should be bypassed');
                        })
                        ->ifNone('bar')
                ],
                [
                    'foo' => 'bar'
                ],
                []
            ],
            'single transform' => [
                [
                    'foo' => 'abc'
                ],
                [
                    (new Item('foo'))->transform(function ($data) {
                        return strtoupper($data);
                    })
                ],
                [
                    'foo' => 'ABC'
                ],
                []
            ],
            'multiple transforms' => [
                [
                    'foo' => 'abc'
                ],
                [
                    (new Item('foo'))->transform(function ($data) {
                        return strtoupper($data);
                    }),
                    (new Item('foo'))->transform(function ($data) {
                        return $data . '/' . $data;
                    })
                ],
                [
                    'foo' => 'ABC/ABC'
                ],
                []
            ],
            'missing required data bypasses transforms' => [
                [],
                [
                    (new Item('foo'))->transform(function () {
                        throw new \Exception('Should be bypassed');
                    })
                ],
                [],
                [
                    'foo' => ['This value is required.']
                ]
            ],
            'default value bypasses transform' => [
                [],
                [
                    (new Item('foo'))
                        ->transform(function ($data) {
                            throw new \Exception('Should be bypassed');
                        })
                        ->ifNone('bar')
                ],
                [
                    'foo' => 'bar'
                ],
                []
            ],
            'mixed config' => [
                [
                    'foo' => 'abc',
                    'bar' => 123,
                    'baz' => true
                ],
                [
                    (new Item('foo'))
                        ->check(function ($data) {
                            return strlen($data) > 5 ? 'Too long.' : null;
                        })
                        ->transform(function ($data) {
                            return strtoupper($data);
                        }),
                    (new Item('bar'))->transform(function ($data) {
                        return $data * 2;
                    }),
                    (new Item('baz'))->ifNone(true),
                    (new Item('qux'))
                ],
                [
                    'foo' => 'ABC',
                    'bar' => 246,
                    'baz' => true
                ],
                [
                    'qux' => ['This value is required.']
                ]
            ],
        ];
    }

    public function collectionProvider()
    {
        return [
            'required collection and empty data' => [
                [],
                [
                    (new Collection('foo', new Item()))
                ],
                [],
                [
                    'foo' => ['This value is required.']
                ]
            ],
            'collection field present but not an array' => [
                [
                    'foo' => true
                ],
                [
                    (new Collection('foo', new Item()))
                ],
                [
                    'foo' => true
                ],
                [
                    'foo' => ['This value must be an array.']
                ]
            ],
            'collection too short' => [
                [
                    'foo' => []
                ],
                [
                    (new Collection('foo', new Item()))->length(2, 3)
                ],
                [
                    'foo' => []
                ],
                [
                    'foo' => ['This value must contain at least 2 element(s).']
                ]
            ],
            'collection too long' => [
                [
                    'foo' => ['a', 'b', 'c', 'd']
                ],
                [
                    (new Collection('foo', new Item()))->length(2, 3)
                ],
                [
                    'foo' => ['a', 'b', 'c', 'd']
                ],
                [
                    'foo' => ['This value must contain at most 3 element(s).']
                ]
            ],
            'failing checks on collection items' => [
                [
                    'foo' => ['a', 'b', 123, 'd', true]
                ],
                [
                    (new Collection('foo', (new Item())->check(function ($item) {
                       return !is_string($item) ? 'This value must be a string.' : null;
                    })))->length(2, 10)
                ],
                [
                    'foo' => ['a', 'b', 123, 'd', true]
                ],
                [
                    'foo/2' => ['This value must be a string.'],
                    'foo/4' => ['This value must be a string.']
                ]
            ],
            'failing checks on item and collection' => [
                [
                    'foo' => [123, 'b']
                ],
                [
                    (new Collection('foo', (new Item())->check(function ($item) {
                       return !is_string($item) ? 'This value must be a string.' : null;
                    })))->length(3, 5)
                ],
                [
                    'foo' => [123, 'b']
                ],
                [
                    'foo' => ['This value must contain at least 3 element(s).'],
                    'foo/0' => ['This value must be a string.']
                ]
            ],
            'passing checks' => [
                [
                    'foo' => ['a', 'b', 'c', 'd']
                ],
                [
                    (new Collection('foo', (new Item())->check(function ($item) {
                       return !is_string($item) ? 'This value must be a string.' : null;
                    })))->length(3, 5)
                ],
                [
                    'foo' => ['a', 'b', 'c', 'd']
                ],
                []
            ],
            'transform on collection' => [
                [
                    'foo' => ['d', 'a', 'c', 'b']
                ],
                [
                    (new Collection('foo', (new Item())))->transform(function ($collection) {
                        sort($collection);
                        $collection[] = 'sorted and augmented';

                        return $collection;
                    })
                ],
                [
                    'foo' => ['a', 'b', 'c', 'd', 'sorted and augmented']
                ],
                []
            ],
            'transform on items' => [
                [
                    'foo' => ['a', 'b', 'c', 'd']
                ],
                [
                    (new Collection('foo', (new Item())->transform(function ($item, $index) {
                        return $index % 2 === 0 ? strtoupper($item) : $item;
                    })))
                ],
                [
                    'foo' => ['A', 'b', 'C', 'd']
                ],
                []
            ],
            'default value' => [
                [],
                [
                    (new Collection('foo', (new Item())))->ifNone(['abc'])
                ],
                [
                    'foo' => ['abc']
                ],
                []
            ],
            'nested collections' => [
                [
                    'foo' => [
                        [[1, 2], [2, 3], [3, 4]],
                        [[5, 6], [7, 8], [9, 10]],
                    ]
                ],
                [
                    (new Collection('foo', (new Item())))->length(2, 3)
                ],
                [
                    'foo' => ['abc']
                ],
                []
            ]
        ];
    }

    private function assertRulesAreFollowed(array $data, array $rules, array $finalData, array $expectedErrors)
    {
        $guard = new Guard();
        $result = $guard->inspect($data, $rules);
        $errors = $result->getErrors();
        $this->assertEquals(count($errors) === 0, $result->isValid());
        $this->assertEquals($expectedErrors, $errors);
        $this->assertEquals($finalData, $result->getData());
    }
}
