<?php

namespace Guard\Tests\Stub;

use Guard\RuleInterface;

class DummyRule implements RuleInterface
{
    private $fieldName;
    private $defaultValue;
    private $required = true;
    private $checks = [];
    private $transforms = [];

    public function __construct(string $fieldName)
    {
        $this->fieldName = $fieldName;
    }

    public function ifNone($defaultValue) : RuleInterface
    {
        $this->required = false;
        $this->defaultValue = $defaultValue;

        return $this;
    }

    public function check(callable $checkFunction) : RuleInterface
    {
        $this->checks[] = $checkFunction;

        return $this;
    }

    public function transform(callable $transformFunction) : RuleInterface
    {
        $this->transforms[] = $transformFunction;

        return $this;
    }

    public function _field(): string
    {
        return $this->fieldName;
    }

    public function _required(): bool
    {
        return $this->required;
    }

    public function _defaultValue()
    {
        return $this->defaultValue;
    }

    public function _checks() : array
    {
        return $this->checks;
    }

    public function _transforms(): array
    {
        return $this->transforms;
    }
}
